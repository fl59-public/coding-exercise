"""
Compare two fasta files and count number of matching sequences
Use to verify solution for coding exercise

Run as:
    python compare_two_fasta.py path_to_test.fasta
"""

from Bio import SeqIO
import sys

# Get path to test fasta from stdin
fasta_test_path = sys.argv[1]
# Path to reference fasta
fasta_ref_path = '../data/longest_isoform_sample.fasta'

def compare_two_fasta(fasta_test_path, fasta_ref_path):
    """
    Compare two multifasta

    Arguments
    --------
    fasta_test_path, fasta_ref_path: str

    Return
    --------
    num_rec, num_match, num_diff, num_absent: int
        number of compared records, matches, differences, and absent in ref

    """
    # Read fasta files into sequence record iterators
    fasta_test = SeqIO.parse(fasta_test_path, 'fasta')
    fasta_ref = SeqIO.parse(fasta_ref_path, 'fasta')
    # Make reference into dictionary to find ref sequences by ID
    # replace duplicate keys; SeqIO.to_dict can't handle dups
    fasta_ref_dict = {rec.id:rec for rec in fasta_ref}
    # Store number of records compared, matches, differences, and absent seqs
    num_rec = 0
    num_match = 0
    num_diff = 0
    num_absent = 0
    # Compare sequences
    for test_record in fasta_test:
        try:
            # Get corresponding reference record
            ref_rec = fasta_ref_dict[test_record.id]
        except KeyError:
            # Or count and skip sequences not found
            num_absent += 1
            continue
        # Compare sequences, and count matches or non-matches
        if ref_rec.seq.upper() == test_record.seq.upper():
            num_match +=1
        else:
            num_diff += 1
        num_rec += 1
    return num_rec, num_match, num_diff, num_absent

if __name__ == "__main__":
    # Compare sequences
    num_rec, num_match,\
    num_diff, num_absent = compare_two_fasta(fasta_test_path, fasta_ref_path)
    # Print result
    result = f"""
    {num_absent} sequences not found in reference fasta

    Of those found:
    {num_match}/{num_rec} sequences match ({num_match/num_rec * 100:0.2f}%)
    {num_diff}/{num_rec} sequences differ ({num_diff/num_rec * 100:0.2f}%)
    """
    print(result)
