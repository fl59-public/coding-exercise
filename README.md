# Auxiliary Repository for VL59 Coding Exercise

## Data
1. longest_isoform_sample.fasta: Sample sequences of longest isoform by feature, used as input to verify result
2. FVL59-CodingExercise-RNAseq.csv: RNA-seq dataset with isoform abundance
3. gencode.v38.annotation.sample.gtf: Sample annotation file with the first 100k lines

## Code
`compare_two_fasta.py`: Verify longest isoform fasta file by comparing to reference
